from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten, Deconv2D
from keras.optimizers import SGD, Adam
from mp1_modif_dataset import *
from mp1_model_plot import *


def linear_model(x_train, y_train, optimizer='sgd', epochs=10, verbose=1):
    """ Create the keras model used for classification and trains it with the given training set

    :param x_train: Input of the training set : shape = (nb_samples, 242000)
    :param y_train: Labels of the training set
    :param optimizer: Chosen optimizer : 'sgd' or 'adam'
    :param epochs: Number of rounds of optimization
    :param verbose: Verbose
    :return: model: The trained model
    """

    x_train = adapt_dataset(x_train)

    model = Sequential()

    model.add(Flatten())
    model.add(Dense(3, activation='softmax'))

    # The optimizer
    if optimizer == 'sgd':
        opt = SGD(lr=0.01,  # Stochastic gradient descent
                  decay=1e-6, momentum=0.9,
                  nesterov=True)
    else:
        opt = Adam(lr=0.001, beta_1=0.9, beta_2=0.999,
                   epsilon=None, decay=0.0, amsgrad=False)

    # The training
    model.compile(loss='categorical_crossentropy',
                  optimizer=opt,
                  metrics=['accuracy'])
    history = model.fit(x_train, y_train,
                        batch_size=32, epochs=epochs, verbose=verbose)

    return model, history


def convolutionnal_model(x_train, y_train, optimizer='sgd', epochs=30, verbose=1):
    """ Create the keras model used for classification and trains it with the given training set

    :param x_train: Input of the training set
    :param y_train: Labels of the training set
    :param optimizer: Chosen optimizer : 'sgd' or 'adam'
    :param epochs: Number of rounds of fitting
    :param verbose: Verbose
    :return: model: The trained model
    """

    x_train = adapt_dataset(x_train)
    shape_x = x_train.shape
    x_train = x_train.reshape(shape_x[0], shape_x[1], shape_x[2], 1)

    model = Sequential()
    model.add(Conv2D(16, (5, 5), activation='relu',
                     input_shape=x_train[0].shape))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(3, activation='softmax'))

    # The optimizer
    if optimizer == 'sgd':
        opt = SGD(lr=0.01,  # Stochastic gradient descent
                  decay=1e-6, momentum=0.9,
                  nesterov=True)
    else:
        opt = Adam(lr=0.001, beta_1=0.9, beta_2=0.999,
                   epsilon=None, decay=0.0, amsgrad=False)

    # The training
    model.compile(loss='categorical_crossentropy',
                  optimizer=opt,
                  metrics=['accuracy'])
    history = model.fit(x_train, y_train,
                        batch_size=32, epochs=epochs, verbose=verbose)

    return model, history


def regression_model(x_train, y_train, optimizer='sgd', epochs=30, verbose=1, learning_rate=0.01):
    """ Create the keras model used for classification and trains it with the given training set

    :param x_train: Input of the training set
    :param y_train: Labels of the training set
    :param optimizer: Chosen optimizer : 'sgd' or 'adam'
    :param epochs: Number of rounds of fitting
    :param verbose: Verbose
    :return: model: The trained model
    """

    shape_x = x_train.shape
    nb_outputs = y_train.shape[1]

    if x_train.shape[1] == 5184:
        im_rows = 72
        im_col = 72
    else:
        im_rows = 100
        im_col = 242

    print("Image size : %s * %d" % (im_rows, im_col))
    x_train = x_train.reshape(shape_x[0], im_rows, im_col, 1)

    model = Sequential()
    model.add(Conv2D(16, (3, 3), activation='relu',
                     input_shape=x_train[0].shape))
    # print(model.output_shape)
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    # print(model.output_shape)
    model.add(Conv2D(16, (2, 2), activation='relu',
                     input_shape=x_train[0].shape))
    # print(model.output_shape)
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    # print(model.output_shape)
    model.add(Conv2D(16, (2, 2), activation='relu',
                     input_shape=x_train[0].shape))
    # print(model.output_shape)
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    # print(model.output_shape)

    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(nb_outputs, activation='relu'))

    # The optimizer
    if optimizer == 'sgd':
        opt = SGD(lr=learning_rate,  # Stochastic gradient descent
                  decay=1e-6, momentum=0.9,
                  nesterov=True)
    else:
        opt = Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999,
                   epsilon=None, decay=0.0, amsgrad=False)

    # The training
    model.compile(loss='categorical_crossentropy',
                  optimizer=opt,
                  metrics=['accuracy'])
    history = model.fit(x_train, y_train,
                        batch_size=32, epochs=epochs, verbose=verbose)

    return model, history


def regression_model2(x_train, y_train, optimizer='sgd', epochs=30, verbose=1, learning_rate=0.01):
    """ Create the keras model used for classification and trains it with the given training set

    :param x_train: Input of the training set
    :param y_train: Labels of the training set
    :param optimizer: Chosen optimizer : 'sgd' or 'adam'
    :param epochs: Number of rounds of fitting
    :param verbose: Verbose
    :return: model: The trained model
    """

    shape_x = x_train.shape
    nb_outputs = y_train.shape[1]

    if x_train.shape[1] == 5184:
        im_rows = 72
        im_col = 72
    else:
        im_rows = 100
        im_col = 242

    print("Image size : %s * %d" % (im_rows, im_col))
    x_train = x_train.reshape(shape_x[0], im_rows, im_col, 1)

    model = Sequential()
    model.add(Conv2D(16, (3, 3), activation='relu',
                     input_shape=x_train[0].shape))
    # print(model.output_shape)
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    # print(model.output_shape)
    model.add(Conv2D(16, (3, 3), activation='relu',
                     input_shape=x_train[0].shape))
    # print(model.output_shape)
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    # print(model.output_shape)
    # model.add(Conv2D(16, (3, 3), activation='relu',
    #                  input_shape=x_train[0].shape))
    # # print(model.output_shape)
    # model.add(MaxPooling2D(pool_size=(2, 2)))
    # model.add(Dropout(0.25))
    print(model.output_shape)

    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    # model.add(Dense(32, activation='relu'))
    # model.add(Dropout(0.5))
    model.add(Dense(nb_outputs, activation='relu'))

    # The optimizer
    if optimizer == 'sgd':
        opt = SGD(lr=learning_rate,  # Stochastic gradient descent
                  decay=1e-6, momentum=0.9,
                  nesterov=True)
    else:
        opt = Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999,
                   epsilon=None, decay=0.0, amsgrad=False)

    # The training
    model.compile(loss='mean_squared_error',
                  optimizer=opt,
                  metrics=['accuracy'])
    history = model.fit(x_train, y_train,
                        batch_size=32, epochs=epochs, verbose=verbose)

    return model, history
