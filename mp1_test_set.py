from mp1_dataset import *
from keras.utils import np_utils


def generate_test_set_classification():
    np.random.seed(42)
    [x_test, y_test] = generate_dataset_classification(300, 20, True)
    return [x_test, y_test]


def generate_test_set_regression():
    np.random.seed(42)
    [x_test, y_test] = generate_dataset_regression(300, 20)
    return [x_test, y_test]


def generate_test_set_denoising():
    np.random.seed(42)
    [x_test, y_test] = generate_dataset_denoising(300, 20, True)
    return [x_test, y_test]

