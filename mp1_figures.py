import matplotlib.pyplot as plt
import numpy as np


def generate_a_drawing(figsize, u, v, noise=0.0):
    """ Generate a figure from a list of points

    :param figsize: Size of the picture
    :param u: horizontal coordinates of the points
    :param v: vertical coordinates of the points
    :param noise: noise to add to the pixels of the figure
    :return: imdata: The pixels of the figure
    """
    fig = plt.figure(figsize=(figsize, figsize))
    ax = plt.subplot(111)
    plt.axis('Off')
    ax.set_xlim(0, figsize)
    ax.set_ylim(0, figsize)
    ax.fill(u, v, "k")
    fig.canvas.draw()
    imdata = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)[::3].astype(np.float32)
    imdata = imdata + noise * np.random.random(imdata.size)
    plt.close(fig)
    return imdata


def generate_a_rectangle(noise=0.0, free_location=False):
    """ Generate a drawing with a rectangle inside it

    :param noise: the noise on the pixels of the figure
    :param free_location: boolean : False if the rectangle must be centered
    :return: imdata: The pixels of the figure
    """
    figsize = 1.0
    u = np.zeros(4)
    v = np.zeros(4)
    if free_location:
        # The rectangle is not centered. We generate 4 random coordinates :
        # 0 & 1 will be the vertical coordinates of the corners,
        # 2 & 3 will be the horizontal ones
        corners = np.random.random(4)
        top = max(corners[0], corners[1])
        bottom = min(corners[0], corners[1])
        left = min(corners[2], corners[3])
        right = max(corners[2], corners[3])
    else:
        # If free_loc == False, the rectangle is centered
        side = (0.3 + 0.7 * np.random.random()) * figsize
        top = figsize / 2 + side / 2
        bottom = figsize / 2 - side / 2
        left = bottom
        right = top
    #  We define the coordinates of the corners : corner_0 = (u[0], v[0])
    u[0] = u[1] = top
    u[2] = u[3] = bottom
    v[0] = v[3] = left
    v[1] = v[2] = right

    # We return the drawing created from those points
    return generate_a_drawing(figsize, u, v, noise)


def generate_a_disk(noise=0.0, free_location=False):
    """ Generate a drawing with a disk inside it

    :param noise: the noise on the pixels of the figure
    :param free_location: boolean : False if the disk must be centered
    :return: imdata: The pixels of the figure
    """
    figsize = 1.0
    if free_location:
        # Randomly place the center
        center = np.random.random(2)
    else:
        center = (figsize / 2, figsize / 2)
    # Random radius
    radius = (0.3 + 0.7 * np.random.random()) * figsize / 2
    n = 50  # Number of points defining the perimeter of the disk
    u = np.zeros(n)
    v = np.zeros(n)
    i = 0
    for t in np.linspace(0, 2 * np.pi, n):
        # We generate n points on the perimeter of the disk
        u[i] = center[0] + np.cos(t) * radius
        v[i] = center[1] + np.sin(t) * radius
        i = i + 1
    # We return the drawing
    return generate_a_drawing(figsize, u, v, noise)


def generate_a_triangle(noise=0.0, free_location=False):
    """ Generate a drawing with a disk inside it

    :param noise: the noise on the pixels of the figure
    :param free_location: boolean : False if the disk must be centered
    :return: imdata: The pixels of the figure
    """
    figsize = 1.0
    if free_location:
        # We generate three random coordinates for three random points
        u = np.random.random(3)
        v = np.random.random(3)
    else:
        # Else, we generate a centered triangle
        size = (0.3 + 0.7 * np.random.random()) * figsize / 2
        middle = figsize / 2
        u = (middle, middle + size, middle - size)
        v = (middle + size, middle - size, middle - size)
    imdata = generate_a_drawing(figsize, u, v, noise)
    # We return the figure data and the coordinates of the three points
    return [imdata, [u[0], v[0], u[1], v[1], u[2], v[2]]]
