from mp1_models import *
from mp1_model_plot import *
from mp1_denoising import *
from mp1_dataset import *
from mp1_figures import *
from mp1_test_set import *
from keras.utils import np_utils
import numpy as np

#############################################
# Images examples

im = generate_a_rectangle(10, True)
plt.imshow(im.reshape(80, 80), cmap='gray')
plt.show()

im = generate_a_disk(10)
plt.imshow(im.reshape(80, 80), cmap='gray')
plt.show()

[im, v] = generate_a_triangle(20, False)
plt.imshow(im.reshape(80, 80), cmap='gray')
plt.show()

#############################################
# Classification examples

[X_train, Y_train] = generate_dataset_classification(300, 20, True)
Y_train = np_utils.to_categorical(Y_train, 3)
model_lin = linear_model(X_train, Y_train, epochs=30)

[X_test, Y_test] = generate_test_set_classification()
width = int(np.sqrt(X_test[0].shape[0]))
X_test = X_test.reshape(X_test.shape[0], width, width)
X_test = X_test[::, ::2, ::2]
# Y_test = np_utils.to_categorical(Y_test, 3)

model_lin.evaluate(X_test, Y_test)

#############################################
# Regression examples

[X_train, Y_train] = generate_dataset_regression(1000, 10)
Y_train = normalize_output_reg3(Y_train)

model_reg, history_reg = regression_model2(X_train, Y_train, 'sgd', epochs=30, verbose=False, learning_rate=0.05)
plot_history(history_reg)

[X_test, Y_test] = generate_test_set_regression()
X_test = X_test.reshape(X_test.shape[0], 72, 72, 1)
Y_test = normalize_output_reg3(Y_test)

print('***** Results on the test dataset *****')
evaluation = model_reg.evaluate(X_test, Y_test, verbose=False)
print('Loss: %s | Accuracy: %s' % (evaluation[0], evaluation[1]))

visualize_prediction(X_test[0], model_reg.predict(np.array([X_test[0]])))
print(model_reg.predict([[X_test[0]]]))

#############################################
# Denoising examples

dataset_size = 50
[X_train, Y_train] = generate_dataset_denoising(dataset_size, 20)
model_denois, history_denois = denoising_model(X_train, Y_train, 'sgd', epochs=10, verbose=False, learning_rate=0.01)
plot_history(history_denois)

[X_test, Y_test] = generate_test_set_denoising()
X_test = X_test.reshape(X_test.shape[0], 72, 72, 1)
evaluation = model_denois.evaluate(X_test, Y_test, verbose=False)
print('***** Results on the test dataset *****')
print('Loss: %s | Accuracy: %s' % (evaluation[0], evaluation[1]))

idx = 0
Y_pred = model_denois.predict(X_test[idx].reshape(1, 72, 72, 1))

plt.figure()
plt.subplot(131)
plt.imshow(Y_test[idx].reshape(72, 72), cmap='gray')
plt.title("Original Image")

plt.subplot(132)
plt.imshow(X_test[idx].reshape(72, 72), cmap='gray')
plt.title("Noisy Image")

plt.subplot(133)
plt.imshow(Y_pred.reshape(72, 72), cmap='gray')
plt.title("Predicted Images")

