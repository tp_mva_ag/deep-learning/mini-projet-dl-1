from mp1_figures import *
from keras.utils import np_utils
import numpy as np
from tqdm import tqdm


def generate_dataset_classification(nb_samples, noise=0.0, free_location=False):
    """ Generates a dataset of figures to train the model

    :param nb_samples: number of figures to create
    :param noise: THe noise to add to each pixel
    :param free_location: boolean, False if not centered figures
    :param verbose: Verbose
    :return: x: The pixels of the figures
    :return: y: The labels : 0=rectangle; 1=disk; 2=triangle
    """
    # Getting im_size:
    im_size = generate_a_rectangle().shape[0]
    x = np.zeros([nb_samples, im_size])
    y = np.zeros(nb_samples)

    #  Randomly create the figures, of rectangles, disks and triangles.
    for i in tqdm(range(nb_samples), desc="Creating dataset"):
        category = np.random.randint(3)
        if category == 0:
            x[i] = generate_a_rectangle(noise, free_location)
        elif category == 1:
            x[i] = generate_a_disk(noise, free_location)
        else:
            [x[i], _] = generate_a_triangle(noise, free_location)
        y[i] = category

    # Adding noise
    x = (x + noise) / (255 + 2 * noise)
    y = np_utils.to_categorical(y, 3)
    return [x, y]


def generate_dataset_regression(nb_samples, noise=0.0):
    # Getting im_size:
    im_size = generate_a_triangle()[0].shape[0]
    x = np.zeros([nb_samples, im_size])
    y = np.zeros([nb_samples, 6])

    for i in tqdm(range(nb_samples), desc="Creating dataset"):
        [x[i], y[i]] = generate_a_triangle(noise, True)

    x = (x + noise) / (255 + 2 * noise)
    return [x, y]


def generate_dataset_denoising(nb_samples, noise=0.0, free_location=False):
    """ Generates a dataset of figures to train the model

    :param nb_samples: number of figures to create
    :param noise: THe noise to add to each pixel
    :param free_location: boolean, False if not centered figures
    :param verbose: Verbose
    :return: x: The pixels of the figures
    :return: y: The labels : 0=rectangle; 1=disk; 2=triangle
    """
    # Getting im_size:
    im_size = generate_a_rectangle().shape[0]
    x = np.zeros([nb_samples, im_size])
    y = np.zeros(nb_samples)

    #  Randomly create the figures, of rectangles, disks and triangles.
    for i in tqdm(range(nb_samples), desc="Creating dataset"):
        category = np.random.randint(3)

        if category == 0:
            image = generate_a_rectangle(0.0, free_location)
        elif category == 1:
            image = generate_a_disk(0.0, free_location)
        else:
            [image, _] = generate_a_triangle(0.0, free_location)
        x[i] = image.copy()

    y = x.copy()
    # Adding noise
    x = x + noise * np.random.random(x.shape)

    y = y / 255.0
    x = (x + noise) / (255.0 + 2 * noise)

    y = y.reshape(nb_samples, 72, 72, 1)

    return [x, y]

