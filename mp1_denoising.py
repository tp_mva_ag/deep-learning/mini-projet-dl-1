from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten, Deconv2D
from keras.optimizers import SGD, Adam


def add_Conv2D(model, filters=16, dim=(3, 3), input_shape=None):

    if input_shape is None:
        input_shape = model.output_shape

    model.add(Conv2D(filters, dim, activation='relu', input_shape=input_shape))
    print("Output shape = " + str(model.output_shape))


def add_Deconv2D(model, filters=16, dim=(3, 3), input_shape=None):

    if input_shape is None:
        input_shape = model.output_shape

    model.add(Deconv2D(filters, dim, activation='relu', input_shape=input_shape))
    print("Output shape = " + str(model.output_shape))


def denoising_model(x_train, y_train, optimizer='sgd', epochs=30, verbose=1, learning_rate=0.01):
    """ Create the keras model used for denoising and trains it with the given training set

    :param x_train: Input of the training set
    :param y_train: Labels of the training set
    :param optimizer: Chosen optimizer : 'sgd' or 'adam'
    :param epochs: Number of rounds of fitting
    :param verbose: Verbose
    :return: model: The trained model
    """

    shape_x = x_train.shape
    nb_outputs = y_train.shape[1]

    if x_train.shape[1] == 5184:
        im_rows = 72
        im_col = 72
    else:
        im_rows = 100
        im_col = 242

    print("Image size : %d * %d" % (im_rows, im_col))
    x_train = x_train.reshape(shape_x[0], im_rows, im_col, 1)

    model = Sequential()
    add_Conv2D(model, input_shape=x_train[0].shape, filters=6, dim=(10, 10))
    add_Conv2D(model, filters=16, dim=(10, 10))
    # add_Conv2D(model)

    # add_Deconv2D(model)
    add_Deconv2D(model, filters=8, dim=(10, 10))
    add_Deconv2D(model, filters=1, dim=(10, 10))

    # The optimizer
    if optimizer == 'sgd':
        opt = SGD(lr=learning_rate,  # Stochastic gradient descent
                  decay=1e-6, momentum=0.9,
                  nesterov=True)
    else:
        opt = Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999,
                   epsilon=None, decay=0.0, amsgrad=False)

    # The training
    model.compile(loss='mean_squared_error',
                  optimizer=opt,
                  metrics=['accuracy'])
    history = model.fit(x_train, y_train,
                        batch_size=32, epochs=epochs, verbose=verbose)

    return model, history
