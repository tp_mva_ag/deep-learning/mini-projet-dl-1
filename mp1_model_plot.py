import matplotlib.pyplot as plt
from matplotlib import patches

def plot_history(history):
    """ Plot the evolution of the accuracy and loss of the model fit

    :param history: The history data
    :return:
    """
    plt.plot(history.history['acc'])
    plt.plot(history.history['loss'])
    plt.ylim([0, 1])
    plt.title('Model accuracy during fitting')
    plt.ylabel('Accuracy / Loss')
    plt.xlabel('epoch')
    plt.legend(['Accuracy', 'Loss'])
    plt.show()


def visualize_prediction(x, y):
    fig, ax = plt.subplots(figsize=(5, 5))
    try:
        i = x.reshape((72, 72))
    except ValueError:
        i = x.reshape((100, 242))

    ax.imshow(i, extent=[-0.15, 1.15, -0.15, 1.15], cmap='gray')
    ax.set_xlim([0, 1])
    ax.set_ylim([0, 1])

    xy = y.reshape(3, 2)
    tri = patches.Polygon(xy, closed=True, fill=False, edgecolor='r', linewidth=5, alpha=0.5)
    ax.add_patch(tri)

    plt.show()
