import numpy as np


def adapt_dataset(x_train):
    """ Adapt the size of the dataset, as the size of images changes from Python to Jupyter...
    It changes the shape to get matrices, and divide the number of pixels

    :param x_train: the original data_set
    :return: x_train_new: The adpated dataset
    """
    # Size reduction
    try:
        x_train_new = x_train.reshape(x_train.shape[0], 100, 242)
        x_train_new = x_train_new[::, ::5, ::5]
    except ValueError:
        width = int(np.sqrt(x_train[0].shape[0]))
        x_train_new = x_train.reshape(x_train.shape[0], width, width)
        x_train_new = x_train_new[::, ::2, ::2]

    return x_train_new


def normalize_output_reg(y):
    """ Normalization of the outputs for the regression

    This function sorts the edges in the following order:
      1. The 'lefter' node (higher x_value)
      2 & 3. The two other nodes, sorted in trigonometric order (counter-clockwise)

    :param y: original outputs
    :return: y_new: [x_mean, y_mean, Y-means]
    """

    y_norm = np.zeros(y.shape)
    # y_norm = np.zeros((y.shape[0], y.shape[1] + 2))
    for i in range(y.shape[0]):
        # We get the horizontal and vertical coordinates : x and y
        y_i = y[i]
        y_x = np.array([y_i[0], y_i[2], y_i[4]])
        y_y = np.array([y_i[1], y_i[3], y_i[5]])

        # The first of this function sort the edges in the following order:
        #   1. The 'lefter' node (higher x_value)
        #   2 & 3. The two other nodes, sorted in trigonometric order (counter-clockwise)

        # The index of the node the most at the left
        idx_first_node = np.argmin(y_x)
        y_norm[i][0] = y_x[idx_first_node]
        y_norm[i][1] = y_y[idx_first_node]

        # List of left nodes to sort
        y_x_left = np.array([y_x[idx] for idx in range(3) if idx != idx_first_node])
        y_y_left = np.array([y_y[idx] for idx in range(3) if idx != idx_first_node])

        # We find the second node as it's angle wrt the first one is the smallest.
        # Thus, the tan of this angle is the smallest, and tan = diff(y) / diff(x)
        idx_second_node = np.argmin([(y_y[idx] - y_y[idx_first_node]) / (y_x[idx] - y_x[idx_first_node])
                                     for idx in range(3) if idx != idx_first_node])

        # We add the second node
        y_norm[i][2] = y_x_left[idx_second_node]
        y_norm[i][3] = y_y_left[idx_second_node]

        # The third node is the last one
        y_norm[i][4] = y_x_left[1 - idx_second_node]
        y_norm[i][5] = y_y_left[1 - idx_second_node]

    return y_norm


def normalize_output_reg2(y, image_size=72, image_reduction=2):
    """ Second try of normalization:
    The output will be a list of pixels, of size the size of the image, with 3 pixels which are equals to '1'

    :param y: the coordinates of the edges
    :return:
    """

    y_shape = int(image_size / image_reduction)
    y_norm = np.zeros((y.shape[0], y_shape * y_shape))

    for i in range(y.shape[0]):
        # We get the horizontal and vertical coordinates : x and y
        y_i = y[i]

        for j in range(3):
            x = int(y_i[2 * j] / image_reduction)
            y = int(y_i[2 * j + 1] / image_reduction)

            y_norm[i][x * y_shape + y] = 1

    return y_norm


def normalize_output_reg3(y):
    """ Third try of normalization:
    The edges are sorted by growing norm

    :param y: the coordinates of the edges
    :return:
    """

    y_norm = np.zeros(y.shape)

    for i in range(y.shape[0]):
        # We get the horizontal and vertical coordinates : x and y
        y_i = y[i]
        y_x = np.array([y_i[0], y_i[2], y_i[4]])
        y_y = np.array([y_i[1], y_i[3], y_i[5]])

        norms = np.sqrt(y_x ** 2 + y_y ** 2)
        sorted_idx = np.argsort(norms)

        # We sort the edges by norm
        y_x_sort = y_x[sorted_idx]
        y_y_sorted = y_y[sorted_idx]

        # We create the normalised Y
        y_norm[i] = [y_x_sort[0], y_y_sorted[0], y_x_sort[1], y_y_sorted[1], y_x_sort[2], y_y_sorted[2]]

    return y_norm


def norm_1_mean(y):
    """ Return a new y parameter which is the mean coordinates of the points

    :param y: original outputs
    :return: y_norm: The mean coordinates wrt x and y
    """
    nb_samples = y.shape[0]
    y_norm = np.zeros(nb_samples, 2)

    for i in range(y.shape[0]):
        y_i = y[i]

        y_i -= np.mean(y_i)
        y_i /= np.std(y_i)

        y_x = np.array([y_i[0], y_i[2], y_i[4]])
        y_y = np.array([y_i[1], y_i[3], y_i[5]])

        y_norm[i] = [np.mean(y_x), np.mean(y_y)]

    return y_norm
